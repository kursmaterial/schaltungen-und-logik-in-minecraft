# Kursstruktur


Die folgende Struktur ist für einen dreistündigen Kurs für Schüler der
weiterführenden Schulen ausgelegt.


> Regelmäßiger Wechsel zwischen Erklärungen mit einem Zeichenprogramm
> und dem Umsetzen der erklärten Konzepte in Minecraft, ist effektiver
> als große Blöcke mit seltenen Wechseln zu haben.


## Grundlagen
- Was ist ein Signal und das Konzept eines Gatters? (Besonders auf
  die Äquivalenz von 0, aus, false und 1, an, true eingehen)
- NOT Gatter und Wahrheitstabelle einführen.
- AND Gatter
- OR Gatter
- AND Gatter kombinieren zu 3-, 4-fach AND


## Vertiefung und Kombination der Grundlagen
- Schaltung bauen bei der genau ein spezifischer Schalter an und ein
  spezifischer Schalter aus sein muss
- Mehr Schalter hinzufügen, die in exakter Position sein müssen
- Aus der Schaltung ein Türschloss mit Schalterkombination bauen
- (Wenn schnell genug: Einen Schalter / Button / etc. einbauen lassen,
  der sich hinter der Tür befindet und dies immer öffnet, ungeachtet der
  Schalterkombination -> Einbau eines OR-Gatters in die Schaltung)


## Fortgeschrittene Konzepte
Jeh nach Geschwindigkeit, Aufnahmefähigkeit und Vorwissen der Teilnehmer
können kein, ein oder mehrere der folgenden Themen behandelt werden:
- Latches und FlipFlops (gerade SR Latch braucht viel Erklärung aber ist
  leicht zu bauen)
- Halbaddierer
- Rätsel stellen in dem eine Wahrheitstabelle angegeben wird, zu welcher
  eine Schaltung gebaut werden muss. Die Schwierigkeit kann durch mehr
  In- und Outputspalten erhöht werden.
- Clocks und Erkennung von Clockflanken
