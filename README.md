# Schaltungen und Logik in Minecraft

## Allgemein
Kursmaterial um Gatter, Logik und Schaltungen in Minecraft zu erklären.
Der Kurs ist dabei so angelegt, dass sich die Teilnehmer zusammen mit
dem Kursleiter auf einem gemeinsamen Server befinden.


## Serveranforderungen
Folgende Serverkonfiguration lief reibungslos mit 4 Teilnehmern und
einem Kursleiter die sich gleichzeitig auf dem Server befanden:
- Minecraft Version: **1.17.1**
- RAM: **3 GB**
- Hauptspeicher: **5 GB SATA SSD (davon nicht mal 1 GB genutzt)**
- CPU: **[nicht bekannt]**


## Setup
Ziel des Setups ist es am Ende einen laufenden Minecraft-Server mit der,
in diesem Repo gespeicherten vorbereiteten Welt zu haben. Dafür kann
grundsätzlich jeder beliebige Hostinganbieter genutzt werden. Hier das
Vorgehen für den Betreiber [minehoster](https://mine-hoster.de):

1. Account erstellen und Server mit oben angegebenen Anforderungen
   konfigurieren und mieten (Stand 2021-11-14 geht das
   [hier](https://mine-hoster.de/minecraft-server-mieten)).


2. Über das [minehoster GamePanel](https://gamepanel.mine-hoster.de) den
   Server einmal starten, dass er sich selber konfiguriert.


3. Minecraft Server stoppen.


4. Über den `Settings` Reiter den FTP Zugang suchen und mit einem FTP
   Client (z.B. Nautilus auf Ubuntu) eine Verbindung herstellen.


5. Den `world`-Ordner auf dem Server durch den `world`-Ordner aus diesem
   Repo ersetzen, der sich im `Server_und_Weltdateien`-Ordner befindet.


6. Entweder die `server.properies` Datei auf dem Server durch die aus
   diesem Repo (`Server_und_Weltdateien/server.properies`) ersetzen oder
   in der Datei die gewünschten Parameter setzten (PVP an/aus, etc.)


7. Minecraft Server starten.


8. Sich selber über die Konsole `operator` rechte geben:
   ```
   op <Playername>
   ```